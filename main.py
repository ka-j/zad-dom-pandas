import pandas as pd

iris = pd.read_csv("data/iris.csv")
with pd.ExcelWriter("data/iris2.xlsx") as writer:
    for var in iris["variety"].unique():
        iris.loc[iris["variety"] == var].to_excel(writer, var)
